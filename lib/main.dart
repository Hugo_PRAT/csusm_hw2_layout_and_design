import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Color color = Theme.of(context).primaryColor;
    Widget titleSection = Container(
      padding: const EdgeInsets.all(32),
      child: Row(
        children: [
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  padding: const EdgeInsets.only(bottom: 8),
                  child: Text('Main page', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ),),
                ),
                Text("description", style: TextStyle(color: Colors.grey[500]),)
              ],
            ),
          ),
          Icon(Icons.home, color: Colors.red[500],),
          Text("11")
        ],
      ),
    );
    Widget buttonSelection = Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          _buildButtonColor(color, Icons.fastfood, 'order'),
          _buildButtonColor(color, Icons.person, 'profile'),
          _buildButtonColor(color, Icons.exit_to_app, 'logout'),
        ],
      ),
    );
    Widget textSection = Container(
      padding: const EdgeInsets.all(22),
      child: ListTile(
        title: Text('Here you can make an oder food among a plenty of choice we have\n'
                    'Just look at the catalogue and choose what you want to try', softWrap: true),
        subtitle: Text('PS: the all order stuff not working yet', softWrap: true),),
      );
    return MaterialApp(
      title: 'Lauyout Homework',
      home: Scaffold(
        appBar: AppBar(
          title: Text('Layout Homework'),
        ),
        body: ListView(
          children: [
            Image.asset('images/frontPage.jpeg',
              width: 600,
              height: 240,
              fit: BoxFit.cover
            ),
            titleSection,
            buttonSelection,
            textSection,
          ],
        ),
      ),
    );
  }

  Column _buildButtonColor(Color color, IconData iconData, String label) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Icon(iconData, color: color,),
        Container(
          margin: const EdgeInsets.only(top: 8),
          child: Text(label, style: TextStyle(fontSize: 12, fontWeight: FontWeight.w400, color: color),),
        ),
      ],
    );
  }
}